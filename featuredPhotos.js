var http = require('http');

http.createServer(function(req,res){

res.writeHead(200,{'content-type':'text/plain'});
var otherArray = ["item1", "item2"];
  var otherObject = { item1: "item1val", item2: "item2val" };
  res.write(
    JSON.stringify({ 
      anObject: otherObject, 
      anArray: otherArray, 
      another: "item",
    })
  );
  res.end();

}).listen(8000);
