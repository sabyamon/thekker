var databaseUrl = "test"; // "username:password@example.com/mydb"
var collections = ["photo_info", "user_info"]
var db = require("mongojs").connect(databaseUrl, collections);

db.user_info.find({gender: "m"}, function(err, users) {
  if( err || !users) console.log("No female users found");
  else users.forEach( function(maleuser) {
    console.log(maleuser);
  });
});


console.log('Now the Sabya is going to log the photo info');

db.photo_info.find(function(err, photos) {
  if( err || !photos) console.log("No Photos");
  else photos.forEach( function(photos) {
    console.log(photos);
  });
});

console.log('I am done !!');